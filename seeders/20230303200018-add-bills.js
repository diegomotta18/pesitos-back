'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    /*await queryInterface.bulkInsert('Bills', [{
      user_id: 1,
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 100.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-01-01'),
      createdAt: new Date('2023-01-01'),
      updatedAt:  new Date('2023-01-01'),
     }], {});
     await queryInterface.bulkInsert('Bills', [{
      user_id: 1,
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 102.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-01-02'),
      createdAt: new Date('2023-01-02'),
      updatedAt:  new Date('2023-01-02'),
     }], {});

     await queryInterface.bulkInsert('Bills', [{
      user_id: 1,
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 103.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-01-03'),
      createdAt: new Date('2023-01-03'),
      updatedAt: new Date('2023-01-03'),
     }], {});

     await queryInterface.bulkInsert('Bills', [{
      user_id: 1,
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 101.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-02-01'),
      createdAt: new Date('2023-02-01'),
      updatedAt:  new Date('2023-02-01'),
     }], {});
     await queryInterface.bulkInsert('Bills', [{
      user_id: 1,
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 103.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-02-02'),
      createdAt: new Date('2023-02-02'),
      updatedAt:  new Date('2023-02-02'),
     }], {});

     await queryInterface.bulkInsert('Bills', [{
      user_id: 1,
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 105.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-02-03'),
      createdAt: new Date('2023-02-03'),
      updatedAt: new Date('2023-02-03'),
     }], {});


     await queryInterface.bulkInsert('Bills', [{
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 103.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-03-01'),
      createdAt: new Date('2023-03-01'),
      updatedAt:  new Date('2023-03-01'),
     }], {});
     await queryInterface.bulkInsert('Bills', [{
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 105.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-03-02'),
      createdAt: new Date('2023-03-02'),
      updatedAt:  new Date('2023-03-02'),
     }], {});

     await queryInterface.bulkInsert('Bills', [{
      user_id: 1,
      category_id: 3,
      waypay_id: 1,
      typebill_id: 1,
      clasification_id: 1,
      amount: 107.50,
      description: 'unadescripcion',
      lastDate: new Date('2023-03-03'),
      createdAt: new Date('2023-03-03'),
      updatedAt: new Date('2023-03-03'),
     }], {});
      */
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     * 
     * 
     */

    await queryInterface.bulkDelete('People', null, {});
  }
};
