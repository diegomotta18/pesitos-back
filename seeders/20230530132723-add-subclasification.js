'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */


    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Vivienda',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Alimentación',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Transporte',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Salud',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Educación',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Entretenimiento',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Vestimenta',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Cuidado personal',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorro e inversiones',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Impuestos',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Viajes y vacaciones',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Mascotas',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Deportes y Actividades físicas',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Otros',
      clasification_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Empleo principal',
      clasification_id: 1,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Trabajo independiente',
      clasification_id: 1,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Emprendimiento',
      clasification_id: 1,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Inversiones',
      clasification_id: 1,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Otros',
      clasification_id: 1,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Vivienda',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Alimentación',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Transporte',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Salud',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Educación',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Entretenimiento',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Vestimenta',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Cuidado personal',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Viajes y vacaciones',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Mascotas',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Deportes',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Otros',
      clasification_id: 4,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para emergencias',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para la jubilación',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para metas a corto plazo',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para la educación',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para inversiones',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
        
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para metas a largo plazo',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para gastos futuros',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para vacaciones',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para la compra de automóviles',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
    await queryInterface.bulkInsert('Subclasifications', [{
      name: 'Ahorros para reducir deudas',
      clasification_id: 3,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
    



  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Subclasifications', null, {});
  }
};
