'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Categories', [{
      name: 'Alquiler',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Categories', [{
      name: 'Expensas',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Categories', [{
      name: 'Pago de hipoteca',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Categories', [{
      name: 'Servicios públicos (agua, electricidad, gas, internet, celular)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Compras de supermercado',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 2,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Comidas fuera de casa (restaurantes, cafeterías, etc)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 2,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Delivery de comida',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 2,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Combustible (si tienes un automóvil)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 3,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Pasajes de transporte público (autobús, tren, metro)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 3,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Taxis o viajes en aplicaciones de transporte',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 3,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Seguro médico o prepaga',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 4,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Consultas médicas',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 4,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Farmacia',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 4,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Tratamientos',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 4,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});



    await queryInterface.bulkInsert('Categories', [{
      name: 'Matrícula y cuotas escolares (si tienes hijos en edad escolar)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 5,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Cursos o capacitaciones adicionales',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 5,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Salidas al cine, teatro, conciertos u otros eventos culturales',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 6,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Suscripciones a plataformas de streaming (Netflix, Spotify, etc.)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 6,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Actividades recreativas (deportes, hobbies, etc.)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 6,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Ropa y calzado',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 7,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Accesorios y complementos',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 7,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Peluquería y cuidado personal',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 8,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Productos de higiene y belleza',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 8,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Ahorro mensual',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 9,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Inversiones financieras (acciones, bonos, fondos de inversión, etc.)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 9,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Seguro de vida o jubilación',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 9,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Impuesto a las Ganancias (si corresponde)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 10,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Impuesto a los Bienes Personales (si corresponde)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 10,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Impuesto al Valor Agregado (IVA) en compras y servicios',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 10,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Pasajes de avión',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Billetes de tren',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Pasajes de autobús',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
    await queryInterface.bulkInsert('Categories', [{
      name: 'Combustible (si viajas en automóvil propio)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Alquiler de vehículos',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Hospedaje en hoteles',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Alquiler de apartamentos o casas vacacionales',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Camping o alojamiento al aire libre',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Comidas en restaurantes y cafeterías durante el viaje',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Compras de víveres para preparar en el lugar de hospedaje',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Entradas a atracciones turísticas (museos, parques temáticos, etc)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Excursiones y tours',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Actividades deportivas (esquí, buceo, surf, etc)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Espectáculos o eventos culturales',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Souvenirs y recuerdos',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Ropa o accesorios adquiridos durante el viaje',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Compras de productos locales',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Pago de un seguro que cubra asistencia médica y otros imprevistos',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name:  'Visas y permisos requeridos para el viaje',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Renovación de pasaporte',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Propinas',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Estacionamiento',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Cambio de divisas',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 11,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});



    await queryInterface.bulkInsert('Categories', [{
      name: 'Alimentación',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 12,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Atención veterinaria',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 12,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Higiene y cuidado',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 12,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Juguetes',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 12,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Accesorios',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 12,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Imprevistos',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 12,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    
    await queryInterface.bulkInsert('Categories', [{
      name: 'Regalos y celebraciones',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 13,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Reparaciones del hogar',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 13,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Compras para el hogar (Accesorios o Electrodomésticos)',
      user_id: 1,
      clasification_id: 2,
      subclasification_id: 13,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
  
    
    await queryInterface.bulkInsert('Categories', [{
      name: 'Salario mensual o quincenal como empleado de una empresa',
      user_id: 1,
      clasification_id: 1,
      subclasification_id: 14,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Ingresos por trabajos a tiempo parcial o temporales',
      user_id: 1,
      clasification_id: 1,
      subclasification_id: 14,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    
    await queryInterface.bulkInsert('Categories', [{
      name: 'Ingresos como freelancer o trabajador autónomo',
      user_id: 1,
      clasification_id: 1,
      subclasification_id: 15,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Categories', [{
      name: 'Honorarios por servicios profesionales',
      user_id: 1,
      clasification_id: 1,
      subclasification_id: 15,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Ingresos generados por un negocio propio (venta de productos, servicios, etc.)',
      user_id: 1,
      clasification_id: 1,
      subclasification_id: 16,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Ganancias de un emprendimiento online (tienda en línea, marketing digital, etc.)',
      user_id: 1,
      clasification_id: 1,
      subclasification_id: 16,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});  

    //TARJETA





    await queryInterface.bulkInsert('Categories', [{
      name: 'Alquiler',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 17,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Categories', [{
      name: 'Expensas',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 17,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Categories', [{
      name: 'Pago de hipoteca',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 17,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Categories', [{
      name: 'Servicios públicos (agua, electricidad, gas, internet, celular)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 17,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Compras de supermercado',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 18,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Comidas fuera de casa (restaurantes, cafeterías, etc)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 18,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Delivery de comida',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 18,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Combustible (si tienes un automóvil)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 19,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Pasajes de transporte público (autobús, tren, metro)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 19,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Taxis o viajes en aplicaciones de transporte',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 19,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Seguro médico o prepaga',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 20,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Consultas médicas',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 20,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Farmacia',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 20,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Tratamientos',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 20,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});



    await queryInterface.bulkInsert('Categories', [{
      name: 'Matrícula y cuotas escolares (si tienes hijos en edad escolar)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 21,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Cursos o capacitaciones adicionales',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 21,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Salidas al cine, teatro, conciertos u otros eventos culturales',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 22,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Suscripciones a plataformas de streaming (Netflix, Spotify, etc.)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 22,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Actividades recreativas (deportes, hobbies, etc.)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 22,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Ropa y calzado',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 23,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Accesorios y complementos',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 23,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Peluquería y cuidado personal',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 24,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Productos de higiene y belleza',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 24,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Ahorro mensual',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 25,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Inversiones financieras (acciones, bonos, fondos de inversión, etc.)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 25,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Seguro de vida o jubilación',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 25,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Impuesto a las Ganancias (si corresponde)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 26,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Impuesto a los Bienes Personales (si corresponde)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 26,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Impuesto al Valor Agregado (IVA) en compras y servicios',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 26,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Pasajes de avión',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Billetes de tren',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Pasajes de autobús',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    
    await queryInterface.bulkInsert('Categories', [{
      name: 'Combustible (si viajas en automóvil propio)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Alquiler de vehículos',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Hospedaje en hoteles',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Alquiler de apartamentos o casas vacacionales',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Camping o alojamiento al aire libre',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Comidas en restaurantes y cafeterías durante el viaje',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Compras de víveres para preparar en el lugar de hospedaje',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Entradas a atracciones turísticas (museos, parques temáticos, etc)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Excursiones y tours',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});


    await queryInterface.bulkInsert('Categories', [{
      name: 'Actividades deportivas (esquí, buceo, surf, etc)',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Espectáculos o eventos culturales',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Souvenirs y recuerdos',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Ropa o accesorios adquiridos durante el viaje',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Compras de productos locales',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Pago de un seguro que cubra asistencia médica y otros imprevistos',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name:  'Visas y permisos requeridos para el viaje',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Renovación de pasaporte',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Propinas',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Estacionamiento',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});

    await queryInterface.bulkInsert('Categories', [{
      name: 'Cambio de divisas',
      user_id: 1,
      clasification_id: 4,
      subclasification_id: 27,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});



  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Categories', null, {});
  }
};
