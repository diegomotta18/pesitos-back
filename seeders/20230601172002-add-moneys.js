'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Moneys', [{
      name: 'Peso argentino',
      symbol: '$',
      createdAt:new Date(),
      updatedAt: new Date(),
     }], {});

     await queryInterface.bulkInsert('Moneys', [{
      name: 'Dólar estadounidense',
      symbol: 'U$D',
      createdAt:new Date(),
      updatedAt: new Date(),
     }], {});
  
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Moneys', null, {});

  }
};
