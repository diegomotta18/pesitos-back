'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
    */
    await queryInterface.bulkInsert('Clasifications', [{
       name: 'Ingresos',
       typebill_id: 1,
       user_id:1,
       createdAt:new Date(),
       updatedAt: new Date(),
     }], {});
     await queryInterface.bulkInsert('Clasifications', [{
      name: 'Gastos',
      typebill_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Clasifications', [{
      name: 'Ahorros',
      typebill_id: 1,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('Clasifications', [{
      name: 'Tarjetas',
      typebill_id: 2,
      user_id:1,
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * 
     */
    await queryInterface.bulkDelete('Clasifications', null, {});
  }
};
