'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('WayPays', [{
      user_id: 1,
      name: 'Efectivo',
      createdAt:new Date(),
      updatedAt: new Date(),
       }], {});
    await queryInterface.bulkInsert('WayPays', [{
      user_id: 1,
      name: 'Tarjeta Debito',
      createdAt:new Date(),
      updatedAt: new Date(),
       }], {});
    await queryInterface.bulkInsert('WayPays', [{
      user_id: 1,
      name: 'Tarjeta Credito',
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
    await queryInterface.bulkInsert('WayPays', [{
      user_id: 1,
      name: 'Transferencia',
      createdAt:new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('WayPays', null, {});
  }
};
