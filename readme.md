# Goal of the project

The goal of the project is to develop "pesitos.app", an application that helps users evaluate their expenses and achieve their financial goals through smart saving. The project is currently in development and utilizes technologies such as React, Hooks, Node.js, and Express.


# Installation Guide

To run the "pesitos" backend on your computer, follow the steps below after installing Docker:

  1 - Clone the project:

    git clone https://gitlab.com/diegomotta18/pesitos-back.git
  
  2 - Install dependencies:
    
    npm install
    
  3 - Run docker compose
  
    docker-compose up -d

  4 - Login using Postman or a similar tool. Send a POST request to the following URL:
  
    http://localhost:3002/auth/login

  In the request body, include the following JSON:

    {
      "email": "devfullstack@gmail.com",
      "password": "admin"
    }

  The response will include a token, which you need to include in the Authorization header as a bearer token for subsequent endpoint  requests.    


