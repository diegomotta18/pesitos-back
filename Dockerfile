FROM node:19

RUN mkdir -p /home/pesitos-back

COPY . /home/pesitos-back
WORKDIR /home/pesitos-back

RUN npm install
RUN npm install -g sequelize-cli
RUN npm remove bcrypt
RUN npm install bcrypt@latest --save   
EXPOSE 3000

CMD [ "npm", "start" ]
