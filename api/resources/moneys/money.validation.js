const Joi = require('@hapi/joi')

const log = require('../utils/logger')

const blueprintMoney = Joi.object({
    name: Joi.string().required(),
    symbol: Joi.string().required(),

})

let validationMoney = (req, res, next) => {
    const resultado = blueprintMoney.validate(req.body, { abortEarly: false, convert: false })
    if (resultado.error === undefined) {
        next()
    } else {
        let errorDeValidacion = resultado.error.details.map(error => {
            return `[${error.message}]`
        })
        log.warn(`La moneda no pasó la validación: ${req.body} - ${errorDeValidacion}   `)
        res.status(400).send(`${errorDeValidacion}`)
    }
}

module.exports = {
    validationMoney
}