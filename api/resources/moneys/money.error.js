class InfoMoneyInUse extends Error {
    constructor(message){
        super(message)
        this.message = message || 'Existe una moneda con el mismo nombre'
        this.status = 409
        this.name = "TypeBillInUse"
    }
}

class MoneyNotExist extends Error {
    constructor(message){
        super(message)
        this.message = message || 'La moneda no existe. Operación no puede ser completada'
        this.status = 404
        this.name = "MoneyNotExist"
    }
}

module.exports = {
    InfoMoneyInUse,
    MoneyNotExist,
}