const Money = require('../../../models').Money;
const { Op } = require("sequelize")

function create(money,user_id) {
    return Money.create({
        user_id: user_id,
        name: money.name,
        symbol: money.symbol
    })
}

async function all(page,pageSize,where,raw = true) {
    
    const options = {
        // Calcula el desplazamiento en función de la página y el tamaño de la página.
        offset: page && pageSize ? (page - 1) * pageSize : undefined,
        // Establece el límite en función del tamaño de la página, o en null para obtener todos los registros.
        limit: page && pageSize ? pageSize : null,
        // Indica si se deben incluir todas las propiedades del modelo o solo algunas.
        attributes: raw ? undefined : ['name'],
      };
      // Realiza la consulta a la base de datos utilizando las opciones definidas.
      const moneys = await Money.findAll(options);
      // Si el formato de salida es personalizado, mapea los registros a un nuevo arreglo de objetos con las propiedades deseadas.
      return raw ? moneys : moneys.map(({ name }) => ({ name}));
}

function findById(id = null) {
    if (!id) throw new Error("No especifico el parametro id para buscar la moneda")
    return new Promise((resolve, reject) => {
        Money.findOne({ where: { id: id } }).
            then(money => {
                resolve(money)
            })
            .catch(err => {
                reject(err)
            })
    })
}

function findByName(name = null) {
    if (id) return Money.findOne({ where: { name: name } })
    throw new Error("No especifico el parametro nombre para buscar la moneda")
}

function moneyExist({ name }) {
    return new Promise((resolve, reject) => {
        Money.findAll({
            where: {
                [Op.or]: [
                    {
                        name: name
                    }
                ]
            }
        }).then(moneys => {
            resolve(moneys.length > 0)
        })
        .catch(err => {
            reject(err)
        })
    })

}

function edit(id, money) {

    return new Promise(function (resolve, reject) {
        Money.update({
            name: money.name,
        }, {
            where: {
                id: id
            }
        }).then(() => {
            let response = findById(id);
            resolve(response);
        });
    })
}

function destroy(id, moneyToDelete) {
    return new Promise(function (resolve, reject) {
        return Money.destroy({
            where: {
                id: id // criteria
            }
        }).then((result) => {
            resolve(moneyToDelete);
        });
    })
}

module.exports = {
    create,
    all,
    edit,
    moneyExist,
    destroy,
    findById,
    findByName
}