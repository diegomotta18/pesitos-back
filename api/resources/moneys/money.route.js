/* Controllers */
const express = require('express')
const passport = require('passport');
const { Op } = require('sequelize');
const log = require('../utils/logger')
const moneyController = require('./money.controller');
const { InfoMoneyInUse , moneyNotExist} = require('./money.error');
const procesarErrores  = require('../../libs/errorHandler').procesarErrores
const validationMoney = require('./money.validation').validationMoney
const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const ExcelJS = require('exceljs');
const fs = require('fs');
const moneyRouter = express.Router()

moneyRouter.post('/', [jwtAuthenticate,validationMoney], procesarErrores((req,res)=>{
    let newMoney = req.body
    const user_id = req.user.id
    return moneyController.moneyExist(newMoney)
    .then(moneyExist =>{
        if(moneyExist){
            log.warn(`Nombre de la moneda [${newMoney.name}] ya existe.`)
            res.status(409).json({message:'Existe una moneda con el mismo nombre.'})
            throw new InfoMoneyInUse()
        }
        return moneyController.create(newMoney,user_id)
        .then(moneyCreated =>{
            res.status(201).json({message:`La moneda con nombre [${moneyCreated.id}] creado exitosamente.`,data:moneyCreated})
        })  
    })
}))

moneyRouter.get("/",[jwtAuthenticate], procesarErrores((req, res) => {
    const {page = 1 ,pageSize ,name} = req.query

    let where = {};

    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    return moneyController.all(page,pageSize,where,true).then(moneys =>{
        res.json({data:moneys})
    })
}))

moneyRouter.put('/:id',[jwtAuthenticate], procesarErrores(async (req, res) => {

    let id = req.params.id
    let moneyToUpdate
    
    moneyToUpdate = await moneyController.findById(id)

    if(!moneyToUpdate){
        throw new  moneyNotExist(`La moneda con id [${id}] no existe.`)
    }

    return moneyController.edit(id,req.body)
    .then(typeBillUpdated => {
            res.json({message:`La moneda con nombre [${typeBillUpdated.name}] ha sido modificado con exito.`,data:typeBillUpdated})
            log.info(`La moneda con nombre [${typeBillUpdated.name}] ha sido modificado con exito.`)
    })
   
}))
moneyRouter.get('/export', [jwtAuthenticate], procesarErrores(async (req, res) => {
    const {page = 1 ,pageSize = Number.MAX_SAFE_INTEGER ,name} = req.query
    let where = {};
    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    const moneys = await moneyController.all(page, pageSize, where);
  
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Monedas');
  
    worksheet.columns = [
      { header: 'ID', key: 'id', width: 10 },
      { header: 'Nombre', key: 'name', width: 30 },
      { header: 'Simbolo', key: 'symbol', width: 30 },
      { header: 'Descripción', key: 'description', width: 50 },
    ];
  
    moneys.rows.forEach((money) => {
      worksheet.addRow({
        id: money.id,
        name: money.name,
        symbol: money.symbol,

        description: money.description,
      });
    });
  
    const filename = 'monedas.xlsx';
    const filepath = `./${filename}`;
    await workbook.xlsx.writeFile(filepath);
  
    const filestream = fs.createReadStream(filepath);
    res.setHeader('Content-Disposition', `attachment; filename="${filename}"`);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    filestream.pipe(res);
  
    filestream.on('close', () => {
      fs.unlinkSync(filepath);
    });
  }));

moneyRouter.delete('/:id', [jwtAuthenticate],procesarErrores((async (req, res) => {

    let id = req.params.id
    let moneyDelete

    moneyDelete = await moneyController.findById(id)
    
    if (!moneyDelete) {
        throw new  moneyNotExist(`La moneda con id [${id}] no existe.`)
    }

    moneyDelete = await moneyController.destroy(id,moneyDelete)
    log.info(`La moneda con id [${id}] fue eliminado.`)
    res.json({message:`La moneda con nombre [${moneyDelete.name}] fue eliminado.`, data:moneyDelete})

})))

moneyRouter.get('/:id',[jwtAuthenticate],procesarErrores((req, res) => {

    let id = req.params.id
    return moneyController.findById(id).then(money =>{
        if(!money){
            throw new moneyNotExist(`La moneda con id [${id}] no existe.`)
        }
        res.json({data:money})
    })
}))
module.exports = moneyRouter