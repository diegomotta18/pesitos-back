const Joi = require('@hapi/joi')

const log = require('../utils/logger')

const blueprintSubclasification = Joi.object({
    name: Joi.string().required(),
    description: Joi.any(),   
    clasification_id: Joi.number().required()
})

let validationSubclasification = (req, res, next) => {
    const resultado = blueprintSubclasification.validate(req.body, { abortEarly: false, convert: false })
    if (resultado.error === undefined) {
        next()
    } else {
        let errorDeValidacion = resultado.error.details.map(error => {
            return `[${error.message}]`
        })
        log.warn(`La subclasificación no pasó la validación: ${req.body} - ${errorDeValidacion}   `)
        res.status(400).send(`${errorDeValidacion}`)
    }
}

module.exports = {
    validationSubclasification
}