const Subclasification = require('../../../models').Subclasification;
const Clasification = require('../../../models').Clasification;

const { Op } = require("sequelize")

function create(subclasification,user_id) {
    return Subclasification.create({
        user_id: user_id,
        name: subclasification.name,
        description: subclasification.description,
        clasification_id: subclasification.clasification_id
    })
}

async function all(page,pageSize,where,raw = true) {
    
    const options = {
        where,
        // Calcula el desplazamiento en función de la página y el tamaño de la página.
        offset: page && pageSize ? (page - 1) * pageSize : undefined,
        // Establece el límite en función del tamaño de la página, o en null para obtener todos los registros.
        limit: page && pageSize ? pageSize : null,
        // Indica si se deben incluir todas las propiedades del modelo o solo algunas.
        attributes: raw ? undefined : ['name'],
        include: [{
            model: Clasification,
            as: 'clasification',
            attributes: ['id','name'],
          }]
      };
      // Realiza la consulta a la base de datos utilizando las opciones definidas.
      const subclasifications = await Subclasification.findAndCountAll(options);
      // Si el formato de salida es personalizado, mapea los registros a un nuevo arreglo de objetos con las propiedades deseadas.
      return raw ? subclasifications : subclasifications.map(({ name }) => ({ name}));
}

function findById(id = null) {
    if (!id) throw new Error("No especifico el parametro id para buscar la subclasificación")
    return new Promise((resolve, reject) => {
        Subclasification.findOne({ where: { id: id } }).
            then(subclasification => {
                resolve(subclasification)
            })
            .catch(err => {
                reject(err)
            })
    })
}

function findByName(name = null) {
    if (id) return Subclasification.findOne({ where: { name: name } })
    throw new Error("No especifico el parametro nombre para buscar la subclasificación")
}

function subclasificationExist({ name }) {
    return new Promise((resolve, reject) => {
        Subclasification.findAll({
            where: {
                [Op.or]: [
                    {
                        name: name
                    }
                ]
            }
        }).then(subclasifications => {
            resolve(subclasifications.length > 0)
        })
        .catch(err => {
            reject(err)
        })
    })

}

function edit(id, subclasification) {

    return new Promise(function (resolve, reject) {
        Subclasification.update({
            user_id: user_id,
            name: subclasification.name,
            description: subclasification.description,
            clasification_id: subclasification.clasification_id
        }, {
            where: {
                id: id
            }
        }).then(() => {
            let response = findById(id);
            resolve(response);
        });
    })
}

function destroy(id, subclasificationToDelete) {
    return new Promise(function (resolve, reject) {
        return Subclasification.destroy({
            where: {
                id: id // criteria
            }
        }).then((result) => {
            resolve(subclasificationToDelete);
        });
    })
}

module.exports = {
    create,
    all,
    edit,
    subclasificationExist,
    destroy,
    findById,
    findByName
}