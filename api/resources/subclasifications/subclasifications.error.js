class InfoSubclasificationInUse extends Error {
    constructor(message){
        super(message)
        this.message = message || 'Existe una subclasificación con el mismo nombre'
        this.status = 409
        this.name = "SubclasificationInUse"
    }
}

class SubclasificationNotExist extends Error {
    constructor(message){
        super(message)
        this.message = message || 'La subclasificación no existe. Operación no puede ser completada'
        this.status = 404
        this.name = "SubclasificationNotExist"
    }
}

module.exports = {
    InfoSubclasificationInUse,
    SubclasificationNotExist,
}