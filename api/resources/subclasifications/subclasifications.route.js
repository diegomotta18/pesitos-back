/* Controllers */
const express = require('express')
const passport = require('passport');
const { Op } = require('sequelize');
const log = require('../utils/logger')
const subclasificationController = require('./subclasifications.controller');
const { InfoSubclasificationInUse , SubclasificationNotExist} = require('./subclasifications.error');
const procesarErrores  = require('../../libs/errorHandler').procesarErrores
const validationSubclasification = require('./subclasifications.validation').validationSubclasification
const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const ExcelJS = require('exceljs');
const fs = require('fs');
const subclasificationRouter = express.Router()

subclasificationRouter.post('/', [jwtAuthenticate,validationSubclasification], procesarErrores((req,res)=>{
    let newSubclasification = req.body
    const user_id = req.user.id
    return subclasificationController.categoryExist(newSubclasification)
    .then(subclasificationExist =>{
        if(subclasificationExist){
            log.warn(`Nombre de la [${newSubclasification.name}] ya existen.`)
            res.status(409).json({message:'Existe una subclasificación con el mismo nombre.'})
            throw new InfoSubclasificationInUse()
        }
        return subclasificationController.create(newSubclasification,user_id)
        .then(subclasificationCreated =>{
            res.status(201).json({message:`Subclasificación con nombre [${subclasificationCreated.id}] creado exitosamente.`,data:subclasificationCreated})
        })  
    })
}))

subclasificationRouter.get("/",[jwtAuthenticate], procesarErrores((req, res) => {
    const {page = 1 ,pageSize ,name,clasification_id} = req.query
    

    let where = {};

    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    if (clasification_id) {
        where.clasification_id =  clasification_id  ;
    }

    return subclasificationController.all(page,pageSize,where).then(subclasifications =>{
        res.json({data:subclasifications.rows,total: subclasifications.count})
    })
}))

subclasificationRouter.put('/:id',[jwtAuthenticate], procesarErrores(async (req, res) => {

    let id = req.params.id
    let subclasificationToUpdate
    
    subclasificationToUpdate = await subclasificationController.findById(id)

    if(!subclasificationToUpdate){
        throw new  SubclasificationNotExist(`La subclasificación con id [${id}] no existe.`)
    }

    return subclasificationController.edit(id,req.body)
    .then(subclasificationUpdated => {
            res.json({message:`La subclasificación con nombre [${subclasificationUpdated.name}] ha sido modificado con exito.`,data:subclasificationUpdated})
            log.info(`La subclasificación con nombre [${subclasificationUpdated.name}] ha sido modificado con exito.`)
    })
   
}))
subclasificationRouter.get('/export', [jwtAuthenticate], procesarErrores(async (req, res) => {
    const {page = 1 ,pageSize = Number.MAX_SAFE_INTEGER ,name} = req.query
    let where = {};
    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    const subclasifications = await subclasificationController.all(page, pageSize, where);
  
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Subclasificación');
  
    worksheet.columns = [
      { header: 'ID', key: 'id', width: 10 },
      { header: 'Nombre', key: 'name', width: 30 },
      { header: 'Descripción', key: 'description', width: 50 },
      { header: 'Clasification ID', key: 'clasificationId', width: 50 },
      { header: 'Clasification', key: 'clasification', width: 50 },
    ];
  
    subclasifications.rows.forEach((subclasification) => {
      worksheet.addRow({
        id: subclasification.id,
        name: subclasification.name,
        description: subclasification.description,
        clasificationId: subclasification.clasification.id,
        clasification: subclasification.clasification.name
      });
    });
  
    const filename = 'subclasificaciones.xlsx';
    const filepath = `./${filename}`;
    await workbook.xlsx.writeFile(filepath);
  
    const filestream = fs.createReadStream(filepath);
    res.setHeader('Content-Disposition', `attachment; filename="${filename}"`);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    filestream.pipe(res);
  
    filestream.on('close', () => {
      fs.unlinkSync(filepath);
    });
  }));

subclasificationRouter.delete('/:id', [jwtAuthenticate],procesarErrores((async (req, res) => {

    let id = req.params.id
    let subclasificationDelete

    subclasificationDelete = await subclasificationController.findById(id)
    
    if (!subclasificationDelete) {
        throw new  SubclasificationNotExist(`La subclasificación con id [${id}] no existe.`)
    }

    subclasificationDelete = await subclasificationController.destroy(id,subclasificationDelete)
    log.info(`La subclasificación con id [${id}] fue eliminado.`)
    res.json({message:`La subclasificación con nombre [${subclasificationDelete.name}] fue eliminado.`, data:subclasificationDelete})

})))

subclasificationRouter.get('/:id',[jwtAuthenticate],procesarErrores((req, res) => {

    let id = req.params.id
    return subclasificationController.findById(id).then(wayPay =>{
        if(!wayPay){
            throw new SubclasificationNotExist(`La subclasificación con id [${id}] no existe.`)
        }
        res.json({data:wayPay})
    })
}))
module.exports = subclasificationRouter