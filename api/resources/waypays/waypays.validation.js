const Joi = require('@hapi/joi')

const log = require('../utils/logger')

const blueprintUser = Joi.object({
    name: Joi.string().required(),
    description: Joi.any(),   
})

let validationWayPay = (req, res, next) => {
    const resultado = blueprintUser.validate(req.body, { abortEarly: false, convert: false })
    if (resultado.error === undefined) {
        next()
    } else {
        let errorDeValidacion = resultado.error.details.map(error => {
            return `[${error.message}]`
        })
        log.warn(`La forma de pago no pasó la validación: ${req.body} - ${errorDeValidacion}   `)
        res.status(400).send(`${errorDeValidacion}`)
    }
}

module.exports = {
    validationWayPay
}