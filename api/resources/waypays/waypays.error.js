class InfoWayPayInUse extends Error {
    constructor(message){
        super(message)
        this.message = message || 'Existe una forma de pago con el mismo nombre'
        this.status = 409
        this.name = "WayPayInUse"
    }
}

class WayPayNotExist extends Error {
    constructor(message){
        super(message)
        this.message = message || 'La forma de pago no existe. Operación no puede ser completada'
        this.status = 404
        this.name = "WayPayNotExist"
    }
}

module.exports = {
    InfoWayPayInUse,
    WayPayNotExist,
}