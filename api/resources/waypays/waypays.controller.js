const wayPayInstance = require('../../../models').WayPay;
const { Op } = require("sequelize")

function create(wayPay,user_id) {
    return wayPayInstance.create({
        user_id: user_id,
        name: wayPay.name,
        description: wayPay.description
    })
}

async function all(page,pageSize,where,raw = true) {
    
    const options = {
        where,
        // Calcula el desplazamiento en función de la página y el tamaño de la página.
        offset: page && pageSize ? (page - 1) * pageSize : undefined,
        // Establece el límite en función del tamaño de la página, o en null para obtener todos los registros.
        limit: page && pageSize ? pageSize : null,
        // Indica si se deben incluir todas las propiedades del modelo o solo algunas.
        attributes: raw ? undefined : ['name'],
      };
      // Realiza la consulta a la base de datos utilizando las opciones definidas.
      const bills = await wayPayInstance.findAndCountAll(options);
      // Si el formato de salida es personalizado, mapea los registros a un nuevo arreglo de objetos con las propiedades deseadas.
      return raw ? bills : bills.map(({ name }) => ({ name}));
}

function findById(id = null) {
    if (!id) throw new Error("No especifico el parametro id para buscar la forma de pago")
    return new Promise((resolve, reject) => {
        wayPayInstance.findOne({ where: { id: id } }).
            then(wayPay => {
                resolve(wayPay)
            })
            .catch(err => {
                reject(err)
            })
    })
}

function findByName(name = null) {
    if (id) return wayPayInstance.findOne({ where: { name: name } })
    throw new Error("No especifico el parametro nombre para buscar la forma de pago")
}

function wayPayExist({ name }) {
    return new Promise((resolve, reject) => {
        wayPayInstance.findAll({
            where: {
                [Op.or]: [
                    {
                        name: name
                    }
                ]
            }
        }).then(wypays => {
            resolve(wypays.length > 0)
        })
        .catch(err => {
            reject(err)
        })
    })

}

function edit(id, wayPay) {

    return new Promise(function (resolve, reject) {
        wayPayInstance.update({
            name: wayPay.name,
            description: wayPay.description
        }, {
            where: {
                id: id
            }
        }).then(() => {
            let response = findById(id);
            resolve(response);
        });
    })
}

function destroy(id, wayPayToDelete) {
    return new Promise(function (resolve, reject) {
        return wayPayInstance.destroy({
            where: {
                id: id // criteria
            }
        }).then((result) => {
            resolve(wayPayToDelete);
        });
    })
}

module.exports = {
    create,
    all,
    edit,
    wayPayExist,
    destroy,
    findById,
    findByName
}