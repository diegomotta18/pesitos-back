/* Controllers */
const express = require('express')
const passport = require('passport')
const log = require('./../utils/logger')
const waypayController = require('./waypays.controller');
const { InfoWayPayInUse , WayPayNotExist} = require('./waypays.error');
const procesarErrores  = require('../../libs/errorHandler').procesarErrores
const validationWayPay = require('./waypays.validation').validationWayPay
const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const ExcelJS = require('exceljs');
const fs = require('fs');
const wayPayRouter = express.Router()

wayPayRouter.post('/', [jwtAuthenticate,validationWayPay], procesarErrores((req,res)=>{
    let newWayPay = req.body
    const user_id = req.user.id
    return waypayController.wayPayExist(newWayPay)
    .then(wayPayExist =>{
        if(wayPayExist){
            log.warn(`Nombre [${newWayPay.name}] ya existen.`)
            res.status(409).json({message:'Existe una forma de pago con el mismo nombre.'})
            throw new InfoWayPayInUse()
        }
        return waypayController.create(newWayPay,user_id)
        .then(wayPayCreated =>{
            res.status(201).json({message:`Forma de pago con nombre [${wayPayCreated.id}] creado exitosamente.`,data:wayPayCreated})
        })  
    })
}))

wayPayRouter.get("/",[jwtAuthenticate], procesarErrores((req, res) => {
    const {page = 1 ,pageSize ,name} = req.query

    let where = {};

    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    return waypayController.all(page,pageSize,where).then(wayPays =>{
        res.json({data:wayPays.rows,total: wayPays.count})
    })
}))

wayPayRouter.get('/export', [jwtAuthenticate], procesarErrores(async (req, res) => {
    const {page = 1 ,pageSize = Number.MAX_SAFE_INTEGER ,name} = req.query
    let where = {};
    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    const wayPays = await waypayController.all(page, pageSize, where);
  
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Formas de pago');
  
    worksheet.columns = [
      { header: 'ID', key: 'id', width: 10 },
      { header: 'Nombre', key: 'name', width: 30 },
      { header: 'Descripción', key: 'description', width: 50 },
    ];
  
    wayPays.rows.forEach((wayPay) => {
      worksheet.addRow({
        id: wayPay.id,
        name: wayPay.name,
        description: wayPay.description,
      });
    });
  
    const filename = 'waypays.xlsx';
    const filepath = `./${filename}`;
    await workbook.xlsx.writeFile(filepath);
  
    const filestream = fs.createReadStream(filepath);
    res.setHeader('Content-Disposition', `attachment; filename="${filename}"`);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    filestream.pipe(res);
  
    filestream.on('close', () => {
      fs.unlinkSync(filepath);
    });
  }));

wayPayRouter.get('/:id',[jwtAuthenticate],procesarErrores((req, res) => {

    let id = req.params.id
    return waypayController.findById(id).then(wayPay =>{
        if(!wayPay){
            throw new WayPayNotExist(`La forma de pago con id [${id}] no existe.`)
        }
        res.json({data:wayPay})
    })
}))

wayPayRouter.put('/:id',[jwtAuthenticate], procesarErrores(async (req, res) => {

    let id = req.params.id
    let wayPayToUpdate
    
    wayPayToUpdate = await waypayController.findById(id)

    if(!wayPayToUpdate){
        throw new  WayPayNotExist(`La forma de pago con id [${id}] no existe.`)
    }

    return waypayController.edit(id,req.body)
    .then(waypayUpdated => {
            res.json({message:`La forma de pago con nombre [${waypayUpdated.name}] ha sido modificado con exito.`,data:waypayUpdated})
            log.info(`La forma de pago con nombre [${waypayUpdated.name}] ha sido modificado con exito.`)
    })
   
}))

wayPayRouter.delete('/:id', [jwtAuthenticate],procesarErrores((async (req, res) => {

    let id = req.params.id
    let wayPayDelete

    wayPayDelete = await waypayController.findById(id)
    
    if (!wayPayDelete) {
        throw new  WayPayNotExist(`La forma de pago con id [${id}] no existe.`)
    }

    wayPayDelete = await waypayController.destroy(id,wayPayDelete)
    log.info(`La forma de pago con id [${id}] fue eliminado.`)
    res.json({message:`La forma de pago con nombre [${wayPayDelete.name}] fue eliminado.`, data:wayPayDelete})

})))

module.exports = wayPayRouter