class InfoTypeBillInUse extends Error {
    constructor(message){
        super(message)
        this.message = message || 'Existe un tipo de movimiento con el mismo nombre'
        this.status = 409
        this.name = "TypeBillInUse"
    }
}

class TypeBillNotExist extends Error {
    constructor(message){
        super(message)
        this.message = message || 'El tipo de movimiento no existe. Operación no puede ser completada'
        this.status = 404
        this.name = "TypeBillNotExist"
    }
}

module.exports = {
    InfoTypeBillInUse,
    TypeBillNotExist,
}