const TypeBill = require('../../../models').TypeBill;
const { Op } = require("sequelize")

function create(typeBill,user_id) {
    return TypeBill.create({
        user_id: user_id,
        name: typeBill.name,
    })
}

async function all(page,pageSize,where,raw = true) {
    
    const options = {
        // Calcula el desplazamiento en función de la página y el tamaño de la página.
        offset: page && pageSize ? (page - 1) * pageSize : undefined,
        // Establece el límite en función del tamaño de la página, o en null para obtener todos los registros.
        limit: page && pageSize ? pageSize : null,
        // Indica si se deben incluir todas las propiedades del modelo o solo algunas.
        attributes: raw ? undefined : ['name'],
      };
      // Realiza la consulta a la base de datos utilizando las opciones definidas.
      const typeBills = await TypeBill.findAll(options);
      // Si el formato de salida es personalizado, mapea los registros a un nuevo arreglo de objetos con las propiedades deseadas.
      return raw ? typeBills : typeBills.map(({ name }) => ({ name}));
}

function findById(id = null) {
    if (!id) throw new Error("No especifico el parametro id para buscar el tipo de movimiento")
    return new Promise((resolve, reject) => {
        TypeBill.findOne({ where: { id: id } }).
            then(typeBill => {
                resolve(typeBill)
            })
            .catch(err => {
                reject(err)
            })
    })
}

function findByName(name = null) {
    if (id) return TypeBill.findOne({ where: { name: name } })
    throw new Error("No especifico el parametro nombre para buscar el tipo de movimiento")
}

function typeBillExist({ name }) {
    return new Promise((resolve, reject) => {
        TypeBill.findAll({
            where: {
                [Op.or]: [
                    {
                        name: name
                    }
                ]
            }
        }).then(typeBills => {
            resolve(typeBills.length > 0)
        })
        .catch(err => {
            reject(err)
        })
    })

}

function edit(id, typeBill) {

    return new Promise(function (resolve, reject) {
        TypeBill.update({
            name: typeBill.name,
        }, {
            where: {
                id: id
            }
        }).then(() => {
            let response = findById(id);
            resolve(response);
        });
    })
}

function destroy(id, typeBillToDelete) {
    return new Promise(function (resolve, reject) {
        return TypeBill.destroy({
            where: {
                id: id // criteria
            }
        }).then((result) => {
            resolve(typeBillToDelete);
        });
    })
}

module.exports = {
    create,
    all,
    edit,
    typeBillExist,
    destroy,
    findById,
    findByName
}