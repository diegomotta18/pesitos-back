const Joi = require('@hapi/joi')

const log = require('../utils/logger')

const blueprintTypeBill = Joi.object({
    name: Joi.string().required(),
})

let validationTypeBill = (req, res, next) => {
    const resultado = blueprintTypeBill.validate(req.body, { abortEarly: false, convert: false })
    if (resultado.error === undefined) {
        next()
    } else {
        let errorDeValidacion = resultado.error.details.map(error => {
            return `[${error.message}]`
        })
        log.warn(`La tipo de movimiento no pasó la validación: ${req.body} - ${errorDeValidacion}   `)
        res.status(400).send(`${errorDeValidacion}`)
    }
}

module.exports = {
    validationTypeBill
}