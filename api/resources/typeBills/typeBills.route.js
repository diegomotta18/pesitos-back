/* Controllers */
const express = require('express')
const passport = require('passport');
const { Op } = require('sequelize');
const log = require('../utils/logger')
const typeBillController = require('./typeBills.controller');
const { InfoTypeBillInUse , typeBillNotExist} = require('./typeBills.error');
const procesarErrores  = require('../../libs/errorHandler').procesarErrores
const validationTypeBill = require('./typeBills.validation').validationTypeBill
const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const ExcelJS = require('exceljs');
const fs = require('fs');
const typeBillRouter = express.Router()

typeBillRouter.post('/', [jwtAuthenticate,validationTypeBill], procesarErrores((req,res)=>{
    let newtypeBill = req.body
    const user_id = req.user.id
    return typeBillController.typeBillExist(newtypeBill)
    .then(typeBillExist =>{
        if(typeBillExist){
            log.warn(`Nombre del tipo de movimiento [${newtypeBill.name}] ya existe.`)
            res.status(409).json({message:'Existe un tipo de movimiento con el mismo nombre.'})
            throw new InfoTypeBillInUse()
        }
        return typeBillController.create(newtypeBill,user_id)
        .then(typeBillCreated =>{
            res.status(201).json({message:`El tipo de movimiento con nombre [${typeBillCreated.id}] creado exitosamente.`,data:typeBillCreated})
        })  
    })
}))

typeBillRouter.get("/",[jwtAuthenticate], procesarErrores((req, res) => {
    const {page = 1 ,pageSize ,name} = req.query

    let where = {};

    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    return typeBillController.all(page,pageSize,where,true).then(typeBills =>{
        res.json({data:typeBills})
    })
}))

typeBillRouter.put('/:id',[jwtAuthenticate], procesarErrores(async (req, res) => {

    let id = req.params.id
    let typeBillToUpdate
    
    typeBillToUpdate = await typeBillController.findById(id)

    if(!typeBillToUpdate){
        throw new  typeBillNotExist(`El tipo de movimiento con id [${id}] no existe.`)
    }

    return typeBillController.edit(id,req.body)
    .then(typeBillUpdated => {
            res.json({message:`El tipo de movimiento con nombre [${typeBillUpdated.name}] ha sido modificado con exito.`,data:typeBillUpdated})
            log.info(`El tipo de movimiento con nombre [${typeBillUpdated.name}] ha sido modificado con exito.`)
    })
   
}))
typeBillRouter.get('/export', [jwtAuthenticate], procesarErrores(async (req, res) => {
    const {page = 1 ,pageSize = Number.MAX_SAFE_INTEGER ,name} = req.query
    let where = {};
    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    const typeBills = await typeBillController.all(page, pageSize, where);
  
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Categorías');
  
    worksheet.columns = [
      { header: 'ID', key: 'id', width: 10 },
      { header: 'Nombre', key: 'name', width: 30 },
      { header: 'Descripción', key: 'description', width: 50 },
    ];
  
    typeBills.rows.forEach((typeBill) => {
      worksheet.addRow({
        id: typeBill.id,
        name: typeBill.name,
        description: typeBill.description,
      });
    });
  
    const filename = 'tipo-de-movimientos.xlsx';
    const filepath = `./${filename}`;
    await workbook.xlsx.writeFile(filepath);
  
    const filestream = fs.createReadStream(filepath);
    res.setHeader('Content-Disposition', `attachment; filename="${filename}"`);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    filestream.pipe(res);
  
    filestream.on('close', () => {
      fs.unlinkSync(filepath);
    });
  }));

typeBillRouter.delete('/:id', [jwtAuthenticate],procesarErrores((async (req, res) => {

    let id = req.params.id
    let typeBillDelete

    typeBillDelete = await typeBillController.findById(id)
    
    if (!typeBillDelete) {
        throw new  typeBillNotExist(`El tipo de movimiento con id [${id}] no existe.`)
    }

    typeBillDelete = await typeBillController.destroy(id,typeBillDelete)
    log.info(`El tipo de movimiento con id [${id}] fue eliminado.`)
    res.json({message:`El tipo de movimiento con nombre [${typeBillDelete.name}] fue eliminado.`, data:typeBillDelete})

})))

typeBillRouter.get('/:id',[jwtAuthenticate],procesarErrores((req, res) => {

    let id = req.params.id
    return typeBillController.findById(id).then(typeBill =>{
        if(!typeBill){
            throw new typeBillNotExist(`El tipo de movimiento con id [${id}] no existe.`)
        }
        res.json({data:typeBill})
    })
}))
module.exports = typeBillRouter