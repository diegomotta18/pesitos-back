const Joi = require('@hapi/joi')

const log = require('../utils/logger')

const blueprintCategory = Joi.object({
    category_id: Joi.any(),
    paymentMethod_id: Joi.number().required(),
    amount: Joi.number().required(),
    description: Joi.any(),
    typebill_id: Joi.number().required(),
    clasification_id: Joi.number().required(),
    subclasification_id: Joi.any(),
    money_id: Joi.number().required(),
    lastDate: Joi.any()
})

let validationBill = (req, res, next) => {
    const resultado = blueprintCategory.validate(req.body, { abortEarly: false, convert: false })
    if (resultado.error === undefined) {
        next()
    } else {
        let errorDeValidacion = resultado.error.details.map(error => {
            return `[${error.message}]`
        })
        log.warn(`el registro de ingreso o egreso no pasó la validación: ${req.body} - ${errorDeValidacion}   `)
        res.status(400).send(`${errorDeValidacion}`)
    }
}

module.exports = {
    validationBill
}