
class BillNotExist extends Error {
    constructor(message){
        super(message)
        this.message = message || 'El registro de ingreso o egreso no existe. Operación no puede ser completada'
        this.status = 404
        this.name = "BillNotExist"
    }
}

module.exports = {
    BillNotExist,
}