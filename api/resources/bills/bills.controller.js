const billInstance = require('../../../models').Bill;
const Category = require('../../../models').Category;
const TypeBill = require('../../../models').TypeBill;
const WayPay = require('../../../models').WayPay;
const Clasification = require('../../../models').Clasification;
const Subclasification = require('../../../models').Subclasification;
const Money = require('../../../models').Money;

const { Op, literal } = require("sequelize")
const { Sequelize } = require("sequelize");
const clasification = require('../../../models/clasification');

function create(bill, user_id) {
    let lastDate = null
    if (bill.lastDate) {
        // Convert the string to a JavaScript Date object
        const dateFormatRegex = /^\d{2}\/\d{2}\/\d{4}$/;
        if (dateFormatRegex.test(bill.lastDate)) {
            const dateParts = (`${bill.lastDate}`).split('/');
            bill.lastDate = new Date(`${dateParts[2]}-${dateParts[1]}-${dateParts[0]}`);
        }
    }
    return billInstance.create({
        user_id: user_id,
        category_id: bill.category_id,
        waypay_id: bill.paymentMethod_id,
        typebill_id: bill.typebill_id,
        clasification_id: bill.clasification_id,
        subclasification_id: bill.subclasification_id,
        amount: bill.amount.toFixed(2),
        description: bill.description,
        lastDate: bill.lastDate,
        money_id: bill.money_id
    })
}

function createImport(bill, user_id) {
    // Convert the string to a JavaScript Date object
    const dateParts = (`${bill.lastDate}`).split('/');
    const lastDate = new Date(`${dateParts[2]}-${dateParts[1]}-${dateParts[0]}`);
    return billInstance.create({
        user_id: user_id,
        typebill_id: bill.typebill_id,
        clasification_id: bill.clasification_id,
        subclasification_id: bill.subclasification_id,
        category_id: bill.category_id,
        waypay_id: bill.waypay_id,
        money_id: bill.money_id,
        description: bill.description,
        lastDate: bill.lastDate,
        amount: bill.amount,
        lastDate: lastDate,
    })
}

async function all(page = 1, pageSize = 10, where, raw = true) {

    const options = {
        offset: page && pageSize ? (page - 1) * pageSize : undefined,
        limit: page && pageSize ? pageSize : null,
        attributes: ['id', 'amount', 'description', [Sequelize.literal(`DATE_FORMAT(lastDate, '%d/%m/%Y')`), 'lastDate'], 'typebill_id', 'user_id', 'category_id', 'waypay_id', 'clasification_id', 'subclasification_id'],
        include: [{
            model: TypeBill,
            as: 'typebill',
            attributes: ['id', 'name'],
        },
        {
            model: Clasification,
            as: 'clasification',
            attributes: ['id', 'name'],
        },
        {
            model: Subclasification,
            as: 'subclasification',
            attributes: ['id', 'name'],
        },
        {
            model: Category,
            as: 'category',
            attributes: ['id', 'name'],
        },
        {
            model: WayPay,
            as: 'waypay',
            attributes: ['id', 'name'],
        },
        {
            model: Money,
            as: 'money',
            attributes: ['id', 'name', 'symbol'],
        }],
        where,
        order: [['createdAt', 'DESC']]

    };
    const bills = await billInstance.findAndCountAll(options);
    return raw ? bills : bills.rows.map(({ amount, description, lastDate, typebill_id, user_id, category_id, waypay_id, clasification_id, subclasification_id, money_id }) => ({ amount, description, lastDate, typebill_id, user_id, category_id, waypay_id, clasification_id, subclasification_id, money_id }));

}

async function allBillsIncomesTotalPerMonth(pageSize, monthR = null, date = null, dateInit = null, dateFinish = null, where) {
    let startDate = null
    let endDate = null

    if (date) {
        startDate = new Date(date);
        const [year, month, day] = startDate.toISOString()
            .substr(0, 10)
            .split("-");
        endDate = new Date(`${year}-${month}-${day}T23:59:59.999Z`);
    }

    if (monthR) {

        const month = monthR > 9 ? `${monthR}` : `0${monthR}`

        startDate = month
            ? new Date(`${new Date().getFullYear()}-${month}-01T00:00:00.000Z`)
            : new Date(`${new Date().getFullYear()}-${new Date().getMonth() + 1}-01T00:00:00.000Z`);

        endDate = month
            ? `${new Date().getFullYear()}-${month}-${new Date(new Date().getFullYear(), month, 0).getDate()}T23:59:59.999Z`
            : `${new Date().getFullYear()}-${new Date().getMonth() + 1}-${new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).getDate()}T23:59:59.999Z`;
    }

    if (dateInit && dateFinish) {
        startDate = new Date(dateInit);
        endDate = new Date(dateFinish);


        console.log(endDate, startDate)
    }

    if (isNaN(Date.parse(startDate)) || isNaN(Date.parse(endDate))) {
        throw new Error("Invalid date format");
    }
    //Balance = (Ingresos totales - Gastos totales - Tarjetas totales) + Ahorros totales


    const results = await billInstance.findAll({
        attributes: [
            [Sequelize.literal("CONCAT(UCASE(LEFT(DATE_FORMAT(createdAt, '%M'), 1)), SUBSTRING(DATE_FORMAT(createdAt, '%M'), 2))"), 'month'],
            [Sequelize.literal("SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END)"), 'total_incomes'],
            [Sequelize.literal("SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END)"), 'total_bills'],
            [Sequelize.literal("SUM(CASE WHEN clasification_id = 1 THEN amount ELSE 0 END)"), 'incomes'],
            [Sequelize.literal("SUM(CASE WHEN clasification_id = 2 THEN amount ELSE 0 END)"), 'expenses'],
            [Sequelize.literal("SUM(CASE WHEN clasification_id = 3 THEN amount ELSE 0 END)"), 'savings'],
            [Sequelize.literal("SUM(CASE WHEN clasification_id = 4 THEN amount ELSE 0 END)"), 'cards'],
            [Sequelize.literal(`
            (SUM(CASE WHEN clasification_id = 1 THEN amount ELSE 0 END) - 
            SUM(CASE WHEN clasification_id = 2 THEN amount ELSE 0 END) - 
            SUM(CASE WHEN clasification_id = 4 THEN amount ELSE 0 END)
            ) + 
            SUM(CASE WHEN clasification_id = 3 THEN amount ELSE 0 END)
        `), 'balance'],
            [Sequelize.literal("(SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END) - SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END))"), 'difference'],
            //[Sequelize.literal("(100 * (SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END) + SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END)) / (SELECT SUM(amount) FROM Bills WHERE createdAt >= :startYear AND createdAt <= :endYear))"), 'percentage'],
            [Sequelize.literal("ROUND((SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END)/SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100, 2)"), 'percentage_spent'],
            [Sequelize.literal("ROUND((100 - (SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END)/SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100), 2)"), 'percentage_not_spent'],
            [
                Sequelize.literal("ROUND((SUM(CASE WHEN clasification_id = 2 THEN amount ELSE 0 END) / SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100, 2)"),
                'percentage_expensives'
            ],
            [
                Sequelize.literal("ROUND((SUM(CASE WHEN clasification_id = 3 THEN amount ELSE 0 END) / SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100, 2)"),
                'percentage_savings'
            ],
            [
                Sequelize.literal("ROUND((SUM(CASE WHEN clasification_id = 4 THEN amount ELSE 0 END) / SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100, 2)"),
                'percentage_cards'
            ]
        ],
        where: {
            lastDate: {
                [Op.gte]: startDate,
                [Op.lte]: endDate,
            },
            ...where
        },
        //replacements: { startYear, endYear },
        group: [Sequelize.literal("DATE_FORMAT(createdAt, '%m')"), Sequelize.literal("CONCAT(UCASE(LEFT(DATE_FORMAT(createdAt, '%M'), 1)), SUBSTRING(DATE_FORMAT(createdAt, '%M'), 2))")],
    });
    const movements = await billInstance.findAndCountAll({
        offset: 0,
        limit: parseInt(pageSize),
        attributes: ['id', 'amount', 'description', [Sequelize.literal(`DATE_FORMAT(lastDate, '%d/%m/%Y')`), 'lastDate'], 'typebill_id', 'user_id', 'category_id', 'waypay_id', 'clasification_id', 'subclasification_id'],
        include: [{
            model: TypeBill,
            as: 'typebill',
            attributes: ['id', 'name'],
        },
        {
            model: Clasification,
            as: 'clasification',
            attributes: ['id', 'name'],
        },
        {
            model: Subclasification,
            as: 'subclasification',
            attributes: ['id', 'name'],
        },
        {
            model: Category,
            as: 'category',
            attributes: ['id', 'name'],
        },
        {
            model: WayPay,
            as: 'waypay',
            attributes: ['id', 'name'],
        },
        {
            model: Money,
            as: 'money',
            attributes: ['id', 'name', 'symbol'],
        }],


        where: {
            lastDate: {
                [Op.gte]: startDate,
                [Op.lte]: endDate,
            },
            user_id: where.user_id
        },
        order: [['createdAt', 'DESC']]


    })

    const transformedResults = results.reduce((acc, cur) => {
        const { month, total_incomes, total_bills, incomes, expenses, savings, dollars, cards, balance, difference, percentage_spent, percentage_not_spent, percentage_savings, percentage_cards, percentage_expensives } = cur.toJSON();

        acc['general'] = {
            total_incomes,
            total_bills,
            incomes,
            expenses,
            savings,
            dollars,
            cards,
            balance,
            difference,
            percentage_spent,
            percentage_not_spent,
            percentage_savings,
            percentage_cards,
            percentage_expensives
        };

        return acc;
    }, {});
    const totalIncomes = transformedResults.general.total_incomes;

    const totalPerCategory = await billInstance.findAll({
        attributes: [
            'subclasification_id',
            [Sequelize.fn('SUM', Sequelize.col('amount')), 'total'],
            [Sequelize.literal('subclasification.name'), 'name'],
            [
                Sequelize.literal(`ROUND((SUM(amount) / ${totalIncomes})*100, 2)`),
                'percentage'
            ]
        ],
        include: [
            {
                model: Subclasification,
                attributes: [],
                as: 'subclasification'
            },
        ],
        where: {
            typebill_id: 2,
            lastDate: {
                [Op.gte]: startDate,
                [Op.lte]: endDate
            },
            ...where
        },
        group: ['subclasification_id'],
    });

    return { transformedResults, movements: movements.rows, totalMovements: movements.count, totalPerCategory: totalPerCategory };

}


async function allRecordByMonthPerYear(year = new Date().getFullYear(), where) {

    const startYear = year
        ? new Date(`${year}-01-01T00:00:00.000Z`)
        : new Date(`${new Date().getFullYear()}-01-01T00:00:00.000Z`);

    const month = new Date().getMonth() + 1 > 9 ? `${new Date().getMonth() + 1}` : `0${new Date().getMonth() + 1}`

    const endYear = year
        ? `${year}-${month}-${new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).getDate()}T23:59:59.999Z`
        : `${new Date().getFullYear()}-0${new Date().getMonth() + 1}-${new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).getDate()}T23:59:59.999Z`;

    const results = await billInstance.findAll({
        attributes: [
            [
                Sequelize.literal("DATE_FORMAT(lastDate, '%Y-%m')"),
                'month'
            ],
            [
                Sequelize.literal("DATE_FORMAT(lastDate, '%M')"),
                'month_name'
            ],
            [
                Sequelize.literal("CAST(ROUND(SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END), 2) AS FLOAT)"),
                'total_incomes'
            ],
            [
                Sequelize.literal("CAST(ROUND(SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END), 2) AS FLOAT)"),
                'total_bills'
            ],
            [
                Sequelize.literal("CAST(ROUND(SUM(CASE WHEN clasification_id = 1 THEN amount ELSE 0 END), 2) AS FLOAT)"),
                'incomes'
            ],
            [
                Sequelize.literal("CAST(ROUND(SUM(CASE WHEN clasification_id = 2 THEN amount ELSE 0 END), 2) AS FLOAT)"),
                'expenses'
            ],
            [
                Sequelize.literal("CAST(ROUND(SUM(CASE WHEN clasification_id = 3 THEN amount ELSE 0 END), 2) AS FLOAT)"),
                'savings'
            ],
            [
                Sequelize.literal("CAST(ROUND(SUM(CASE WHEN clasification_id = 4 THEN amount ELSE 0 END), 2) AS FLOAT)"),
                'cards'
            ],
            [
                Sequelize.literal("CAST(ROUND(SUM(CASE WHEN clasification_id = 5 THEN amount ELSE 0 END), 2) AS FLOAT)"),
                'dollars'
            ],
            [
                Sequelize.literal("CAST(ROUND(SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END) - SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END), 2) AS FLOAT)"),
                'difference'
            ],
            [
                Sequelize.literal("CAST(ROUND((SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END)/SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100, 2) AS FLOAT)"),
                'percentage_spent'
            ],
            [
                Sequelize.literal("CAST(ROUND((100 - (SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END)/SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100), 2) AS FLOAT)"),
                'percentage_not_spent'
            ],
            [
                Sequelize.literal("CAST(ROUND((SUM(CASE WHEN clasification_id = 2 THEN amount ELSE 0 END) / SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100, 2) AS FLOAT)"),
                'percentage_expenses'
            ],
            [
                Sequelize.literal("CAST(ROUND((SUM(CASE WHEN clasification_id = 3 THEN amount ELSE 0 END) / SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100, 2) AS FLOAT)"),
                'percentage_savings'
            ],
            [
                Sequelize.literal("CAST(ROUND((SUM(CASE WHEN clasification_id = 4 THEN amount ELSE 0 END) / SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END))*100, 2) AS FLOAT)"),
                'percentage_cards'
            ]
        ],
        where: {
            lastDate: {
                [Op.gte]: startYear,
                [Op.lte]: endYear
            },
            ...where
        },
        group: [
            Sequelize.literal("DATE_FORMAT(lastDate, '%Y-%m')"),
            Sequelize.literal("DATE_FORMAT(lastDate, '%M')")
        ],
        order: [
            [Sequelize.literal("DATE_FORMAT(lastDate, '%Y-%m')"), 'ASC']
        ]
    });



    const resultsSpent = await billInstance.findAll({
        attributes: [
            [Sequelize.literal("SUM(CASE WHEN typebill_id = 2 THEN amount ELSE 0 END)"), 'total_spent'],
        ],
        where: {
            lastDate: {
                [Op.gte]: startYear,
                [Op.lte]: endYear,
            },
            ...where
        },

    });

    const resultsNotSpent = await billInstance.findAll({
        attributes: [
            [Sequelize.literal("SUM(CASE WHEN typebill_id = 1 THEN amount ELSE 0 END)"), 'total_not_spent'],
        ],
        where: {
            lastDate: {
                [Op.gte]: startYear,
                [Op.lte]: endYear,
            },
            ...where
        },
    });



    const totalPerCategory = await billInstance.findAll({
        attributes: [
            'subclasification_id',
            [Sequelize.fn('SUM', Sequelize.col('amount')), 'total'],
            [Sequelize.literal('subclasification.name'), 'name'],
            [
                Sequelize.literal(`ROUND((SUM(amount) / ${resultsNotSpent[0].dataValues.total_not_spent})*100, 2)`),
                'percentage'
            ]
        ],
        include: [
            {
                model: Subclasification,
                attributes: [],
                as: 'subclasification'
            },
        ],
        where: {
            typebill_id: 2,
            lastDate: {
                [Op.gte]: startYear,
                [Op.lte]: endYear,
            },
            ...where
        },
        group: ['subclasification_id'],
    });



    const finalResults = { recordMonths: results, spent: parseFloat(resultsSpent[0].dataValues.total_spent), notSpent: parseFloat(resultsNotSpent[0].dataValues.total_not_spent) }
    finalResults.percentage_spent = parseFloat((finalResults.spent / (finalResults.spent + finalResults.notSpent) * 100).toFixed(2));
    finalResults.percentage_not_spent = parseFloat((100 - finalResults.percentage_spent).toFixed(2));
    finalResults.totalPerCategory = totalPerCategory;
    finalResults.accumulatedBalance = (parseFloat(resultsNotSpent[0]?.dataValues.total_not_spent) - parseFloat(resultsSpent[0]?.dataValues.total_spent) || 0).toFixed(2);
    return finalResults
}

function findById(id = null, user = null) {
    if (!id) throw new Error("No especifico el parametro id para buscar el registro de ingreso o egreso")
    return new Promise((resolve, reject) => {
        billInstance.findOne(
            {
                attributes: ['id', 'amount', 'description', [Sequelize.literal(`DATE_FORMAT(lastDate, '%Y-%m-%dT00:00:00')`), 'lastDate'], 'typebill_id', 'user_id', 'category_id', 'waypay_id', 'clasification_id', 'subclasification_id', 'money_id'],
                where: { id: id, user_id: user },
                include: [{
                    model: TypeBill,
                    as: 'typebill',
                    attributes: ['id', 'name'],
                },
                {
                    model: Clasification,
                    as: 'clasification',
                    attributes: ['id', 'name'],
                },
                {
                    model: Subclasification,
                    as: 'subclasification',
                    attributes: ['id', 'name'],
                },
                {
                    model: Category,
                    as: 'category',
                    attributes: ['id', 'name'],
                },
                {
                    model: WayPay,
                    as: 'waypay',
                    attributes: ['id', 'name'],
                }]
            }).
            then(bill => {
                resolve(bill)
            })
            .catch(err => {
                reject(err)
            })
    })
}

function findByName(name = null) {
    if (id) return billInstance.findOne({ where: { name: name } })
    throw new Error("No especifico el parametro nombre para buscar el registro de ingreso o egreso")
}

function billExist({ name }) {
    return new Promise((resolve, reject) => {
        billInstance.findAll({
            where: {
                [Op.or]: [
                    {
                        name: name
                    }
                ]
            }
        }).then(bills => {
            resolve(bills.length > 0)
        })
            .catch(err => {
                reject(err)
            })
    })

}

function edit(id, bill, user_id) {
    console.log('user_id edit', user_id)

    return new Promise(function (resolve, reject) {
        billInstance.update({
            category_id: bill.category_id,
            waypay_id: bill.paymentMethod_id,
            typebill_id: bill.typebill_id,
            clasification_id: bill.clasification_id,
            subclasification_id: bill.subclasification_id,
            amount: bill.amount,
            description: bill.description,
            lastDate: bill.lastDate,
            money_id: bill.money_id
        },
            {
                where: {
                    id: id,
                    user_id: user_id
                }
            }).then(() => {
                let response = findById(id, user_id);
                resolve(response);
            });
    })
}

function destroy(id, billToDelete) {
    return new Promise(function (resolve, reject) {
        return billInstance.destroy({
            where: {
                id: id // criteria
            }
        }).then((result) => {
            resolve(billToDelete);
        });
    })
}

module.exports = {
    create,
    all,
    edit,
    billExist,
    destroy,
    findById,
    findByName,
    allBillsIncomesTotalPerMonth,
    allRecordByMonthPerYear,
    createImport
}