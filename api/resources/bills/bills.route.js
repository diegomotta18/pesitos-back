/* Controllers */
const express = require('express')
const passport = require('passport')
const log = require('./../utils/logger')
const billController = require('./bills.controller');
const procesarErrores  = require('../../libs/errorHandler').procesarErrores
const validationBill = require('./bills.validation').validationBill
const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const { BillNotExist } = require('./bills.error');
const ExcelJS = require('exceljs');
const fs = require('fs');
const { Op } = require('sequelize');


const billRouter = express.Router()



const xlsx = require('xlsx')
const path = require('path');
const multer = require('multer');
const storage = multer.memoryStorage()
const upload = multer({ storage: storage })

billRouter.post('/', [jwtAuthenticate,validationBill], procesarErrores((req,res)=>{
    let bill = req.body
    const user_id = req.user.id

    return billController.create(bill,user_id)
    .then(billCreated =>{
        res.status(201).json({message:`Movimiento creado exitosamente.`,data:billCreated})
    })  
    
}))

billRouter.get("/",[jwtAuthenticate], procesarErrores((req, res) => {
  const { page = 1, pageSize = 10, name, money = 1 } = req.query;
  const user_id = req.user.id
//   let where = { money_id: money,user_id:user_id };
  let where = { user_id:user_id };

  if (name && name !== '') {
      where = {
          ...where,
          [Op.or]: [
              { '$typebill.name$': { [Op.like]: `%${name}%` } },
              { '$clasification.name$': { [Op.like]: `%${name}%` } },
              { '$category.name$': { [Op.like]: `%${name}%` } },
              { '$waypay.name$': { [Op.like]: `%${name}%` } }
          ]
      };
  }

  return billController.all(page,pageSize,where,true).then(wayPays =>{
      res.json({data:wayPays.rows,total: wayPays.count})
  })
}))



billRouter.get("/allByMonth",[jwtAuthenticate], procesarErrores((req, res) => {
    let {pageSize = 10,dateCurrent,month , dateInit,dateEnd, money = 1} = req.query
    const user_id = req.user.id
    let where = { money_id: money,user_id:user_id };

    return billController.allBillsIncomesTotalPerMonth(pageSize,month,dateCurrent,dateInit,dateEnd,where).then(bills =>{
        res.json({data:bills})
    })

}))

billRouter.get("/allByYear",[jwtAuthenticate], procesarErrores((req, res) => {
    let record = req.body
    let {money = 1} = req.query
    const user_id = req.user.id
    let where = { money_id: money,user_id:user_id };

    return billController.allRecordByMonthPerYear(record.year,where).then(bills =>{
        res.json({data:bills})
    })
    
}))
billRouter.get('/export', [jwtAuthenticate], procesarErrores(async (req, res) => {
    const {page = 1 ,pageSize = Number.MAX_SAFE_INTEGER ,name} = req.query
    const user_id = req.user.id
    let where = { user_id:user_id };
    if (name) {
        where = {
            ...where,
            name: { [Op.like]: `%${name}%` }
        };
    }
    
    const bills = await billController.all(page, pageSize, where,true);
  
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Movimientos');
  
    worksheet.columns = [
        { header: 'ID Movimiento', key: 'id', width: 30 },
        { header: 'ID Tipo', key: 'typebill_id', width: 30 },
        { header: 'Tipo', key: 'typebill', width: 30 },
        { header: 'ID Clasificación', key: 'clasification_id', width: 30 },
        { header: 'Clasification', key: 'clasification', width: 30 },
        { header: 'ID Subclasificación', key: 'subclasification_id', width: 30 },
        { header: 'Subclasificación', key: 'subclasification', width: 30 },
        { header: 'ID Categoría', key: 'category_id', width: 30 },
        { header: 'Categoría', key: 'category', width: 30 },
        { header: 'ID Forma de pago', key: 'waypay_id', width: 30 },
        { header: 'Forma de pago', key: 'waypay', width: 30 },
        { header: 'Descripción', key: 'description', width: 30 },
        { header: 'Fecha', key: 'lastDate', width: 30 },
        { header: 'Monto', key: 'amount', width: 30 },
        { header: 'ID Moneda', key: 'money_id', width: 30 },
        { header: 'Moneda', key: 'money', width: 30 },

    ];
    
    bills.rows.forEach((bill) => {
      worksheet.addRow({
        id: bill.id,
        clasification_id: bill.clasification_id != null ? bill.clasification.id : '',
        clasification: bill.clasification != null ? bill.clasification.name : '',
        subclasification_id: bill.subclasification_id != null ? bill.subclasification.id : '',
        subclasification: bill.subclasification != null ? bill.subclasification.name : '',
        category_id: bill.category != null ? bill.category.id : '',
        category: bill.category != null ? bill.category.name : '',
        waypay_id: bill.waypay != null ? bill.waypay.id : '', 
        waypay: bill.waypay != null ? bill.waypay.name : '', 
        typebill: bill.typebill != null ? bill.typebill.name : '',
        typebill_id: bill.typebill != null ? bill.typebill.id : '',
        description: bill.description != null ? bill.description : '',
        lastDate: bill.lastDate,
        amount: bill.amount,
        money_id: bill.money.id,
        money: bill.money.name

      });
    });
  
    const filename = 'movimientos.xlsx';
    const filepath = `./${filename}`;
    await workbook.xlsx.writeFile(filepath);
  
    const filestream = fs.createReadStream(filepath);
    res.setHeader('Content-Disposition', `attachment; filename="${filename}"`);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    filestream.pipe(res);
  
    filestream.on('close', () => {
      fs.unlinkSync(filepath);
    });
  }));

billRouter.get('/:id',[jwtAuthenticate],procesarErrores((req, res) => {

    let id = req.params.id
    const user_id = req.user.id
    return billController.findById(id,user_id).then(bill =>{
        if(!bill){
            throw new BillNotExist(`El Movimiento con id [${id}] no existe.`)
        }
        res.json({data:bill})
    })
}))

billRouter.put('/:id',[jwtAuthenticate], procesarErrores(async (req, res) => {

    let id = req.params.id
    let billToUpdate
    let user = req.user.id
    billToUpdate = await billController.findById(id,user) 

    if(!billToUpdate){
        throw new  BillNotExist(`El Movimiento con id [${id}] no existe.`)
    }

    return billController.edit(id,req.body,user)
    .then(billUpdated => {
        res.json({message:`El Movimiento con id [${billUpdated.id}] ha sido modificado con exito.`,data:billUpdated})
        log.info(`El Movimiento con id [${billUpdated.id}] ha sido modificado con exito.`)
    })
   
}))

billRouter.delete('/:id', [jwtAuthenticate],procesarErrores((async (req, res) => {

    let id = req.params.id
    const user_id = req.user.id

    let billDelete

    billDelete = await billController.findById(id,user_id)
    
    if (!billDelete) {
        throw new  BillNotExist(`El Movimiento con id[${id}] no existe.`)
    }

    billDelete = await billController.destroy(id,billDelete)
    log.info(`El Movimiento con id [${id}] fue eliminado.`)
    res.json({message:`El Movimiento con id [${billDelete.id}] fue eliminado.`, data:billDelete})

})))


billRouter.post('/import',  [jwtAuthenticate], upload.single('file'),procesarErrores((async (req, res) => {
    const user_id = req.user.id

    const file = req.file
  
    if (!file) {
      res.status(400).json({ message: 'Se requiere un archivo Excel.' })
      return
    }
  
  // Verifica que el archivo no esté vacío
    if (file.size === 0) {
        res.status(400).json({ message: 'El archivo está vacío.' })
        return
    }
  
    // Verifica la extensión del archivo
    const fileExtension = path.extname(file.originalname)
    if (fileExtension !== '.xls' && fileExtension !== '.xlsx') {
        res.status(400).json({ message: 'El archivo debe tener una extensión .xls o .xlsx.' })
        return
    }
    
    const workbook = xlsx.read(file.buffer, { type: 'buffer' })
    const sheetName = workbook.SheetNames[0]
    const worksheet = workbook.Sheets[sheetName]
  
  
      // Valida que el archivo tenga los encabezados correctos
    const expectedHeaders = [
        'ID Movimiento',
        'ID Tipo',
        'Tipo',
        'ID Clasificación',
        'Clasification',
        'ID Subclasificación',
        'Subclasificación',
        'ID Categoría',
        'Categoría',
        'ID Forma de pago',
        'Forma de pago',
        'Descripción',
        'Fecha',
        'Monto',
        'ID Moneda',
        'Moneda'
    ]
    const headers = xlsx.utils.sheet_to_json(worksheet, { header: 1 })[0]
    const invalidHeaders = headers.filter(header => !expectedHeaders.includes(header))
    if (invalidHeaders.length > 0) {
        res.status(400).json({ message: `El archivo Excel contiene encabezados inválidos: ${invalidHeaders.join(', ')}` })
        return
    }
  
    const rows = xlsx.utils.sheet_to_json(worksheet, { header: 1 })
  
    // Elimina la fila de encabezado
    rows.shift()
    // Convierte las filas en objetos de movimientos
    const movements = rows.map(row => ({
      
      typebill_id: row[1] != '' ? row[1] : null,
      clasification_id:  row[3] != '' ? row[3] : null,
      subclasification_id:  row[5] != '' ? row[5] : null,
      category_id:  row[7] != '' ? row[7] : null,
      waypay_id:  row[9] != '' ? row[9] : null,
      description:  row[11] != '' ? row[11] : null,
      lastDate:  row[12] != '' ? row[12] : null,
      amount: row[13] != '' ? row[13] : null,
      money_id: row[14] != '' ? row[14] : null,

    }))
  
    const promises = movements.map(async (mov) => {
  
        return billController.createImport(mov,user_id)
    })
  
    // Espera a que se completen todas las promesas
    await Promise.all(promises)
  
    res.json({ message: 'Importación completada exitosamente.' })
  })))

module.exports = billRouter