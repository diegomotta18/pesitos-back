class InfoCategoryInUse extends Error {
    constructor(message){
        super(message)
        this.message = message || 'Existe una categoria con el mismo nombre'
        this.status = 409
        this.name = "CategoryInUse"
    }
}

class CategoryNotExist extends Error {
    constructor(message){
        super(message)
        this.message = message || 'La categoria no existe. Operación no puede ser completada'
        this.status = 404
        this.name = "CategoryNotExist"
    }
}

module.exports = {
    InfoCategoryInUse,
    CategoryNotExist,
}