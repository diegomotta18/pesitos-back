const categoryInstance = require('../../../models').Category;
const { Op } = require("sequelize");
const clasification = require('../../../models').Clasification;
const subclasification = require('../../../models').Subclasification;

function create(category,user_id) {
    return categoryInstance.create({
        user_id: user_id,
        name: category.name,
        description: category.description,
        clasification_id: category.clasification,
        subclasification_id: category.subclasification
    })
}

async function all(page,pageSize,where,raw = true) {
    
    const options = {
        where,
        // Calcula el desplazamiento en función de la página y el tamaño de la página.
        offset: page && pageSize ? (page - 1) * parseInt(pageSize) : undefined,
        // Establece el límite en función del tamaño de la página, o en null para obtener todos los registros.
        limit: page && pageSize ? parseInt(pageSize) : null,
        // Indica si se deben incluir todas las propiedades del modelo o solo algunas.
        include: [
            {
                model: clasification,
                as: 'clasification',
                attributes: ['id','name'],
            },
            {
                model: subclasification,
                as: 'subclasification',
                attributes: ['id','name'],
            }
        ],
        attributes: raw ? undefined : ['name'],
      };
      // Realiza la consulta a la base de datos utilizando las opciones definidas.
      const categories = await categoryInstance.findAndCountAll(options);
      // Si el formato de salida es personalizado, mapea los registros a un nuevo arreglo de objetos con las propiedades deseadas.
      return raw ? categories : categories.map(({ name }) => ({ name}));
}

function findById(id = null,user_id = null) {
    console.log('findById ', user_id)
    if (!id) throw new Error("No especifico el parametro id para buscar la categoria")
    return new Promise((resolve, reject) => {
        categoryInstance.findOne({ where: { id: id , user_id:user_id} }).
            then(category => {
                resolve(category)
            })
            .catch(err => {
                reject(err)
            })
    })
}

function findByName(name = null,user_id) {
    if (id) return categoryInstance.findOne({ where: { name: name ,user_id:user_id} })
    throw new Error("No especifico el parametro nombre para buscar la categoria")
}

function categoryExist({ name }) {
    return new Promise((resolve, reject) => {
        categoryInstance.findAll({
            where: {
                [Op.or]: [
                    {
                        name: name
                    }
                ]
            }
        }).then(categories => {
            resolve(categories.length > 0)
        })
        .catch(err => {
            reject(err)
        })
    })

}

function edit(id, category,user_id) {

    return new Promise(function (resolve, reject) {
        categoryInstance.update({
            name: category.name,
            description: category.description,
            clasification_id: category.clasification,
            subclasification_id: category.subclasification

        }, {
            where: {
                id: id,
                user_id: user_id
            }
        }).then(() => {
            let response = findById(id,user_id);
            resolve(response);
        });
    })
}

function destroy(id, categoryToDelete) {
    return new Promise(function (resolve, reject) {
        return categoryInstance.destroy({
            where: {
                id: id // criteria
            }
        }).then((result) => {
            resolve(categoryToDelete);
        });
    })
}

module.exports = {
    create,
    all,
    edit,
    categoryExist,
    destroy,
    findById,
    findByName
}