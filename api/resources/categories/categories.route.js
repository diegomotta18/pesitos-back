/* Controllers */
const express = require('express')
const passport = require('passport');
const { Op } = require('sequelize');
const log = require('./../utils/logger')
const categoryController = require('./categories.controller');
const { InfoCategoryInUse , CategoryNotExist} = require('./categories.error');
const procesarErrores  = require('../../libs/errorHandler').procesarErrores
const validationCategory = require('./categories.validation').validationCategory
const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const ExcelJS = require('exceljs');
const fs = require('fs');
const categoryRouter = express.Router()

categoryRouter.post('/', [jwtAuthenticate,validationCategory], procesarErrores((req,res)=>{
    let newCategory = req.body
    const user_id = req.user.id
    return categoryController.categoryExist(newCategory)
    .then(categoryExist =>{
        if(categoryExist){
            log.warn(`Nombre de la [${newCategory.name}] ya existen.`)
            res.status(409).json({message:'Existe una categoria con el mismo nombre.'})
            throw new InfoCategoryInUse()
        }
        return categoryController.create(newCategory,user_id)
        .then(categoryCreated =>{
            res.status(201).json({message:`Categoria con nombre [${categoryCreated.id}] creado exitosamente.`,data:categoryCreated})
        })  
    })
}))

categoryRouter.get("/",[jwtAuthenticate], procesarErrores((req, res) => {
    const {page = 1 ,pageSize ,name,clasification_id,subclasification_id} = req.query
    
    const user_id = req.user.id
    let where = { user_id:user_id };

    if (name) {
        where = {
            ...where,
            name: { [Op.like]: `%${name}%` }
        };
    }

    if (subclasification_id) {
        where.subclasification_id =  subclasification_id  ;
    }

    return categoryController.all(page,pageSize,where).then(wayPays =>{
        res.json({data:wayPays.rows,total: wayPays.count})
    })
}))

categoryRouter.put('/:id',[jwtAuthenticate], procesarErrores(async (req, res) => {

    let id = req.params.id
    let user_id = req.user.id
    let categoryToUpdate
    console.log('categoryRouter.put ', user_id)

    categoryToUpdate = await categoryController.findById(id,user_id)

    if(!categoryToUpdate){
        throw new  CategoryNotExist(`La categoria con id [${id}] no existe.`)
    }

    return categoryController.edit(id,req.body,user_id)
    .then(categoryUpdated => {
            res.json({message:`La categoria con nombre [${categoryUpdated.name}] ha sido modificado con exito.`,data:categoryUpdated})
            log.info(`La categoria con nombre [${categoryUpdated.name}] ha sido modificado con exito.`)
    })
   
}))
categoryRouter.get('/export', [jwtAuthenticate], procesarErrores(async (req, res) => {
    const {page = 1 ,pageSize = Number.MAX_SAFE_INTEGER ,name} = req.query
    const user_id = req.user.id
    let where = { user_id:user_id };

    if (name) {
        where = {
            ...where,
            name: { [Op.like]: `%${name}%` }
        };
    }
    const categories = await categoryController.all(page, pageSize, where);
  
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Categorías');
  
    worksheet.columns = [
      { header: 'ID', key: 'id', width: 10 },
      { header: 'Nombre', key: 'name', width: 30 },
      { header: 'Descripción', key: 'description', width: 50 },
    ];
  
    categories.rows.forEach((category) => {
      worksheet.addRow({
        id: category.id,
        name: category.name,
        description: category.description,
      });
    });
  
    const filename = 'categorias.xlsx';
    const filepath = `./${filename}`;
    await workbook.xlsx.writeFile(filepath);
  
    const filestream = fs.createReadStream(filepath);
    res.setHeader('Content-Disposition', `attachment; filename="${filename}"`);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    filestream.pipe(res);
  
    filestream.on('close', () => {
      fs.unlinkSync(filepath);
    });
  }));

categoryRouter.delete('/:id', [jwtAuthenticate],procesarErrores((async (req, res) => {

    let id = req.params.id
    const user_id = req.user.id

    let categoryDelete

    categoryDelete = await categoryController.findById(id,user_id)
    
    if (!categoryDelete) {
        throw new  CategoryNotExist(`La categoria con id [${id}] no existe.`)
    }

    categoryDelete = await categoryController.destroy(id,categoryDelete)
    log.info(`La categoria con id [${id}] fue eliminado.`)
    res.json({message:`La categoria con nombre [${categoryDelete.name}] fue eliminado.`, data:categoryDelete})

})))

categoryRouter.get('/:id',[jwtAuthenticate],procesarErrores((req, res) => {

    let id = req.params.id
    const user_id = req.user.id

    return categoryController.findById(id,user_id).then(category =>{
        if(!category){
            throw new CategoryNotExist(`La categoria con id [${id}] no existe.`)
        }
        res.json({data:category})
    })
}))
module.exports = categoryRouter