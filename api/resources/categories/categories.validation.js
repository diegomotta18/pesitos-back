const Joi = require('@hapi/joi')

const log = require('../utils/logger')

const blueprintCategory = Joi.object({
    name: Joi.string().required(),
    clasification: Joi.number().required(),
    subclasification: Joi.number().required(),
    description: Joi.any(),   
})

let validationCategory = (req, res, next) => {
    const resultado = blueprintCategory.validate(req.body, { abortEarly: false, convert: false })
    if (resultado.error === undefined) {
        next()
    } else {
        let errorDeValidacion = resultado.error.details.map(error => {
            return `[${error.message}]`
        })
        log.warn(`La categoria no pasó la validación: ${req.body} - ${errorDeValidacion}   `)
        res.status(400).send(`${errorDeValidacion}`)
    }
}

module.exports = {
    validationCategory
}