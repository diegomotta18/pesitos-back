class InfoClasificationInUse extends Error {
    constructor(message){
        super(message)
        this.message = message || 'Existe una clasificación con el mismo nombre'
        this.status = 409
        this.name = "ClasificationInUse"
    }
}

class ClasificationNotExist extends Error {
    constructor(message){
        super(message)
        this.message = message || 'La clasificación no existe. Operación no puede ser completada'
        this.status = 404
        this.name = "ClasificationNotExist"
    }
}

module.exports = {
    InfoClasificationInUse,
    ClasificationNotExist,
}