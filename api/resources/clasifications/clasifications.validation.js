const Joi = require('@hapi/joi')

const log = require('../utils/logger')

const blueprintClasification = Joi.object({
    name: Joi.string().required(),
    description: Joi.any(),   
    typebill_id: Joi.number().required()
})

let validationClasification = (req, res, next) => {
    const resultado = blueprintClasification.validate(req.body, { abortEarly: false, convert: false })
    if (resultado.error === undefined) {
        next()
    } else {
        let errorDeValidacion = resultado.error.details.map(error => {
            return `[${error.message}]`
        })
        log.warn(`La clasificación no pasó la validación: ${req.body} - ${errorDeValidacion}   `)
        res.status(400).send(`${errorDeValidacion}`)
    }
}

module.exports = {
    validationClasification
}