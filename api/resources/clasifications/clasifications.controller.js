const Clasification = require('../../../models').Clasification;
const TypeBill = require('../../../models').TypeBill;

const { Op } = require("sequelize")

function create(clasification,user_id) {
    return Clasification.create({
        user_id: user_id,
        name: clasification.name,
        description: clasification.description,
        typebill_id: clasification.typebill_id
    })
}

async function all(page,pageSize,where,raw = true) {
    
    const options = {
        where,
        // Calcula el desplazamiento en función de la página y el tamaño de la página.
        offset: page && pageSize ? (page - 1) * pageSize : undefined,
        // Establece el límite en función del tamaño de la página, o en null para obtener todos los registros.
        limit: page && pageSize ? pageSize : null,
        // Indica si se deben incluir todas las propiedades del modelo o solo algunas.
        attributes: raw ? undefined : ['name'],
        include: [{
            model: TypeBill,
            as: 'typebill',
            attributes: ['id','name'],
          }]
      };
      // Realiza la consulta a la base de datos utilizando las opciones definidas.
      const clasifications = await Clasification.findAndCountAll(options);
      // Si el formato de salida es personalizado, mapea los registros a un nuevo arreglo de objetos con las propiedades deseadas.
      return raw ? clasifications : clasifications.map(({ name }) => ({ name}));
}

function findById(id = null) {
    if (!id) throw new Error("No especifico el parametro id para buscar la clasificación")
    return new Promise((resolve, reject) => {
        Clasification.findOne({ where: { id: id } }).
            then(clasification => {
                resolve(clasification)
            })
            .catch(err => {
                reject(err)
            })
    })
}

function findByName(name = null) {
    if (id) return Clasification.findOne({ where: { name: name } })
    throw new Error("No especifico el parametro nombre para buscar la clasificación")
}

function clasificationExist({ name }) {
    return new Promise((resolve, reject) => {
        Clasification.findAll({
            where: {
                [Op.or]: [
                    {
                        name: name
                    }
                ]
            }
        }).then(categories => {
            resolve(categories.length > 0)
        })
        .catch(err => {
            reject(err)
        })
    })

}

function edit(id, clasification) {

    return new Promise(function (resolve, reject) {
        Clasification.update({
            user_id: user_id,
            name: clasification.name,
            description: clasification.description,
            typebill_id: clasification.typebill_id
        }, {
            where: {
                id: id
            }
        }).then(() => {
            let response = findById(id);
            resolve(response);
        });
    })
}

function destroy(id, clasificationToDelete) {
    return new Promise(function (resolve, reject) {
        return Clasification.destroy({
            where: {
                id: id // criteria
            }
        }).then((result) => {
            resolve(clasificationToDelete);
        });
    })
}

module.exports = {
    create,
    all,
    edit,
    clasificationExist,
    destroy,
    findById,
    findByName
}