/* Controllers */
const express = require('express')
const passport = require('passport');
const { Op } = require('sequelize');
const log = require('./../utils/logger')
const clasificationController = require('./clasifications.controller');
const { InfoClasificationInUse , ClasificationNotExist} = require('./clasifications.error');
const procesarErrores  = require('../../libs/errorHandler').procesarErrores
const validationClasification = require('./clasifications.validation').validationClasification
const jwtAuthenticate = passport.authenticate('jwt', { session: false })
const ExcelJS = require('exceljs');
const fs = require('fs');
const clasificationRouter = express.Router()

clasificationRouter.post('/', [jwtAuthenticate,validationClasification], procesarErrores((req,res)=>{
    let newClasification = req.body
    const user_id = req.user.id
    return clasificationController.categoryExist(newClasification)
    .then(clasificationExist =>{
        if(clasificationExist){
            log.warn(`Nombre de la [${newClasification.name}] ya existen.`)
            res.status(409).json({message:'Existe una clasificación con el mismo nombre.'})
            throw new InfoClasificationInUse()
        }
        return clasificationController.create(newClasification,user_id)
        .then(clasificationCreated =>{
            res.status(201).json({message:`Clasificación con nombre [${clasificationCreated.id}] creado exitosamente.`,data:clasificationCreated})
        })  
    })
}))

clasificationRouter.get("/",[jwtAuthenticate], procesarErrores((req, res) => {
    const {page = 1 ,pageSize ,name} = req.query

    let where = {};

    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    return clasificationController.all(page,pageSize,where).then(wayPays =>{
        res.json({data:wayPays.rows,total: wayPays.count})
    })
}))

clasificationRouter.put('/:id',[jwtAuthenticate], procesarErrores(async (req, res) => {

    let id = req.params.id
    let categoryToUpdate
    
    categoryToUpdate = await clasificationController.findById(id)

    if(!categoryToUpdate){
        throw new  ClasificationNotExist(`La clasificación con id [${id}] no existe.`)
    }

    return clasificationController.edit(id,req.body)
    .then(categoryUpdated => {
            res.json({message:`La clasificación con nombre [${categoryUpdated.name}] ha sido modificado con exito.`,data:categoryUpdated})
            log.info(`La clasificación con nombre [${categoryUpdated.name}] ha sido modificado con exito.`)
    })
   
}))
clasificationRouter.get('/export', [jwtAuthenticate], procesarErrores(async (req, res) => {
    const {page = 1 ,pageSize = Number.MAX_SAFE_INTEGER ,name} = req.query
    let where = {};
    if (name) {
        where.name = { [Op.like]: `%${name}%` };
    }

    const clasifications = await clasificationController.all(page, pageSize, where);
  
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('Clasificación');
  
    worksheet.columns = [
      { header: 'ID', key: 'id', width: 10 },
      { header: 'Nombre', key: 'name', width: 30 },
      { header: 'Descripción', key: 'description', width: 50 },
      { header: 'Tipo de movimiento', key: 'typebill', width: 50 },
    ];
  
    clasifications.rows.forEach((clasification) => {
      worksheet.addRow({
        id: clasification.id,
        name: clasification.name,
        description: clasification.description,
        typebill: clasification.typebill.name
      });
    });
  
    const filename = 'clasificaciones.xlsx';
    const filepath = `./${filename}`;
    await workbook.xlsx.writeFile(filepath);
  
    const filestream = fs.createReadStream(filepath);
    res.setHeader('Content-Disposition', `attachment; filename="${filename}"`);
    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    filestream.pipe(res);
  
    filestream.on('close', () => {
      fs.unlinkSync(filepath);
    });
  }));

clasificationRouter.delete('/:id', [jwtAuthenticate],procesarErrores((async (req, res) => {

    let id = req.params.id
    let clasificationDelete

    clasificationDelete = await clasificationController.findById(id)
    
    if (!clasificationDelete) {
        throw new  ClasificationNotExist(`La clasificación con id [${id}] no existe.`)
    }

    clasificationDelete = await clasificationController.destroy(id,clasificationDelete)
    log.info(`La clasificación con id [${id}] fue eliminado.`)
    res.json({message:`La clasificación con nombre [${clasificationDelete.name}] fue eliminado.`, data:clasificationDelete})

})))

clasificationRouter.get('/:id',[jwtAuthenticate],procesarErrores((req, res) => {

    let id = req.params.id
    return clasificationController.findById(id).then(wayPay =>{
        if(!wayPay){
            throw new ClasificationNotExist(`La clasificación con id [${id}] no existe.`)
        }
        res.json({data:wayPay})
    })
}))
module.exports = clasificationRouter