'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Clasification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Clasification.belongsTo(models.TypeBill,{foreignKey:"typebill_id",targetKey:"id", as:"typebill"})
      Clasification.belongsTo(models.User,{foreignKey:"user_id",targetKey:"id", as:"user"})

    }
  }
  Clasification.init({
    user_id: DataTypes.BIGINT,
    typebill_id: DataTypes.BIGINT,
    name: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
    sequelize,
    timestamps: true,
    paranoid: true,
    modelName: 'Clasification',
  });
  return Clasification;
};