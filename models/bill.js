'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Bill extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Bill.belongsTo(models.User,{foreignKey:"user_id",targetKey:"id", as:"user"})
      Bill.belongsTo(models.Category,{foreignKey:"category_id",targetKey:"id", as:"category"})
      Bill.belongsTo(models.WayPay,{foreignKey:"waypay_id",targetKey:"id", as:"waypay"})
      Bill.belongsTo(models.Clasification,{foreignKey:"clasification_id",targetKey:"id", as:"clasification"})
      Bill.belongsTo(models.Subclasification,{foreignKey:"subclasification_id",targetKey:"id", as:"subclasification"})
      Bill.belongsTo(models.TypeBill,{foreignKey:"typebill_id",targetKey:"id", as:"typebill"})
      Bill.belongsTo(models.Money,{foreignKey:"money_id",targetKey:"id", as:"money"})

    }
  }
  Bill.init({
    user_id: DataTypes.BIGINT,
    category_id: DataTypes.BIGINT,
    waypay_id: DataTypes.BIGINT,
    typebill_id: DataTypes.BIGINT,
    clasification_id: DataTypes.BIGINT,
    subclasification_id: DataTypes.BIGINT,
    money_id: DataTypes.BIGINT,
    amount: DataTypes.DECIMAL,
    description: DataTypes.TEXT,
    lastDate: DataTypes.DATE,
    deletedAt: DataTypes.DATE
  }, {
    sequelize,
    timestamps: true,
    paranoid: true,
    modelName: 'Bill',
  });
  return Bill;
};