'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Category.belongsTo(models.User,{foreignKey:"user_id",targetKey:"id", as:"user"})
      Category.belongsTo(models.Clasification,{foreignKey:"clasification_id",targetKey:"id", as:"clasification"})
      Category.belongsTo(models.Subclasification,{foreignKey:"subclasification_id",targetKey:"id", as:"subclasification"})

    }
  }
  Category.init({
    user_id: DataTypes.BIGINT,
    clasification_id: DataTypes.BIGINT,
    subclasification_id: DataTypes.BIGINT,
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    deletedAt: DataTypes.DATE
  }, {
    sequelize,
    timestamps: true,
    paranoid: true,
    modelName: 'Category',
  });
  return Category;
};