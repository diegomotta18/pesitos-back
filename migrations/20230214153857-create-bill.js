'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Bills', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      user_id: {
        type: Sequelize.BIGINT,
        reference: 'Users',
        referenceKey: 'id'        
      },
      category_id: {
        type: Sequelize.BIGINT,
        reference: 'Categories',
        referenceKey: 'id'      
      },
      waypay_id: {
        type: Sequelize.BIGINT,
        reference: 'WayPays',
        referenceKey: 'id'   
      },
      typebill_id: {
        type: Sequelize.BIGINT,
        reference: 'TypeBill',
        referenceKey: 'id'   
      },
      clasification_id: {
        type: Sequelize.BIGINT,
        reference: 'Clasification',
        referenceKey: 'id'   
      },
      subclasification_id: {
        type: Sequelize.BIGINT,
        reference: 'Subclasification',
        referenceKey: 'id'   
      },
      amount: {
        type: Sequelize.DECIMAL(10, 2),

      },
      description: {
        allowNull: true,
        type: Sequelize.TEXT
      },
      lastDate: {
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Bills');
  }
};