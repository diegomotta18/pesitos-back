const express = require('express')
const bodyParser = require('body-parser')
const authRouter = require('./api/resources/auth/auth.route')
const usersRouter = require('./api/resources/users/users.route')
const wayPaysRouter = require('./api/resources/waypays/waypays.route')
const categoriesRouter = require('./api/resources/categories/categories.route')
const billRouter = require('./api/resources/bills/bills.route')
const typeBillRouter = require('./api/resources/typeBills/typeBills.route')
const clasificationRouter = require('./api/resources/clasifications/clasifications.route')
const subclasificationRouter = require('./api/resources/subclasifications/subclasifications.route')
const moneyRouter = require('./api/resources/moneys/money.route')

const morgan = require('morgan');
const logger = require("./api/resources/utils/logger")
//autenticacion basica
// const auth = require('./api/libs/auth').basicStrategyLogin
// const BasicStrategy = require('passport-http').BasicStrategy
const authJTW = require('./api/libs/auth')
const config = require('./api/config')
const passport = require('passport')
const errorHandler = require('./api/libs/errorHandler')

const cors = require('cors');


const app = express()

app.use(cors())
app.use(morgan('short',{
    stream:{
        write: message => logger.info(message.trim())
    }
}))

passport.use(authJTW)

app.use(bodyParser.json())

app.use(passport.initialize())

app.use('/auth',authRouter)
app.use('/users',usersRouter)
app.use('/paymentMethods',wayPaysRouter)
app.use('/categories',categoriesRouter)
app.use('/bills',billRouter)

app.use('/typeBills',typeBillRouter)
app.use('/clasifications',clasificationRouter)
app.use('/subclasifications',subclasificationRouter)
app.use('/moneys',moneyRouter)



app.use(errorHandler.procesarErrores)

if(config.ambiente === 'prod'){
    app.use(errorHandler.erroresEnProduccion)
}else{
    app.use(errorHandler.erroresEnDesarrollo)
}


// app.get('/',(_req,res) => {
//     return res.send('conectado')
// })

app.listen(3000,()=>console.log('listening...pesitos!!!'))


//RUN MIGRATRION

//docker exec clubpedidos-back-app-1  sequelize db:migrate --env development